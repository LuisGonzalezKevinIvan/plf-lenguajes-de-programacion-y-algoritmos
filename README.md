# Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)  
```plantuml
@startmindmap
*[#Crimson] Programando ordenadores en los 80 y ahora.
**[#MediumOrchid] Sistemas
***[#DodgerBlue] Actuales
**** Son faciles de adquirir en cualquier tienda.
**** Facilidad al programar en diversas maquinas.
***[#DodgerBlue] Antiguos 
****[#DeepSkyBlue] No han tenido continuidad a nuevas generaciones
****[#DeepSkyBlue] Decada de los 80´s  y 90´s
***** Limitacion de los recursos.
***** Necesario conocer las diferentes arquitecturas de las maquinas.
***** Cuello de botellas en hardware.

**[#MediumOrchid] Diferencias
*** Potencia en las maquinas.
*** Diferencia de arquitecturas.
*** Velocidad en los procesadores.
*** Actualmente mejores precios.

**[#MediumOrchid] Lenguajes
***[#DodgerBlue] Alto nivel
****[#DeepSkyBlue] Traduccion a codigo maquina.
****[#DeepSkyBlue] Lo realizan los compiladores de interpretes.
****[#DeepSkyBlue] Desventajas
***** Sobrecarga en las capas.
***** Programadores desconocen el funcionamiento del hardware.
****[#DeepSkyBlue] Ejemplos
***** Java
***** Fortran
***** C**
***[#DodgerBlue] Bajo nivel
****[#DeepSkyBlue] Orientado a procesos
****[#DeepSkyBlue] Ensamblador
***** Aprovechamiento de los recursos.

**[#MediumOrchid] Leyes 
***[#DodgerBlue] Page
**** El software se vuelve el doble de lento cada 18 meses.
***[#DodgerBlue] Moore 
**** Cada 18 meses se puede producir una cpu el doble de rapida.

@endmindmap
```

# Historia de los algoritmos y de los lenguajes de programación (2010)

```plantuml
@startmindmap

*[#Crimson] Historia de los algoritmos y de los\n lenguajes de programación
**[#MediumOrchid] Programa
*** Operaciones especificadas en un lenguaje de programacion.
***_ Se ejecuta en:
****[#DodgerBlue] Dispositivos 
****[#DodgerBlue] Maquinas 
*****[#DeepSkyBlue] Siglo XVII
****** Mecanicas 
****** No programables 
*****[#DeepSkyBlue] Siglo XIX
****** Primer computadora
******* Maquina analitica
******** Operaciones basicas
********* Resta 
********* Suma
********* Multiplicacion
********* Division

**[#MediumOrchid] Lenguajes de programacion
***[#DodgerBlue] Paradigma
****[#DeepSkyBlue] Funcional 
***** Trabaja con la idea de la simplificacion.
*****_ Años 60's Surge:
******[#Cyan] LISP
******[#Cyan] Otros lenguajes
******* Erlang
******* Haskell
******* Miranda
****[#DeepSkyBlue] Orientado a objetos 
***** Organiza el diseño entorno a objetos y datos.
*****_ En 1968 surge:
******[#Cyan] Simula
******[#Cyan] Otros lenguajes
******* Python 
******* Ruby
******* PHP
****[#DeepSkyBlue] Programacion logica
***** Procesos paralelos distribuidos
*****_ En 1971 surge:
******[#Cyan] Prolog
******[#Cyan] Otros lenguajes
******* Mercury
******* Godel 
******* ALF 
***[#DodgerBlue] Lenguajes mas destacados
**** Java
**** Fortran
**** C**

**[#MediumOrchid] Algoritmo
***_ Es:
**** Lista ordenada, definida y finita de operaciones que encuentran la solucion a un problema.
***[#DodgerBlue] Ejemplos
**** Manuales de usuario
**** Recetas de cocina 
***[#DodgerBlue] Tipos 
****[#DeepSkyBlue] Razonables
***** El tiempo de ejecucion crece despacio a medida que los problemas se hacen mas grandes.
****[#DeepSkyBlue] No razonables 
***** Añadir un dato mas al problema hace que se duplique el tiempo.
***[#DodgerBlue] Historia 
****[#DeepSkyBlue] 3000 a.C
***** Tablillas de arcilla para calcular el capital.
****[#DeepSkyBlue] Siglo XVII
***** Se registran las primeras ayudas mecanicas en forma de calculadoras.
****[#DeepSkyBlue] Siglo XIX
***** Primeras maquinas programales.
@endtmindmap
```

# Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)

```plantuml
@startmindmap
*[#Crimson] Evolución de los lenguajes y\n paradigmas de programación
**[#MediumOrchid] Lenguaje de programacion
***_ Medio de comunicacion hombre * maquina.
***_ Innovar para resolver problemas diversos y\n complejos facilitando la eficiencia de ejecucion.

**[#MediumOrchid] Programacion estructurada 
*** Consiste en analizar una funcion principal,\ndescomponer a funciones mas sencillas de resolver.
***[#DodgerBlue] Lenguajes
**** Algol 
**** Basic 
**** C
**** Pascal
**** Fortran 
**** Java 

**[#MediumOrchid] Paradigma 
***_ tipos:
****[#DodgerBlue] Logico 
***** Terminos de premisas y conclusiones.
***** Usa la recursividad.
***** Dominio de problemas mediante una serie de axiomas logicos.
*****[#DeepSkyBlue] Lenguajes 
****** Prolog 
****** Mercury 
****** ALF    
****[#DodgerBlue] Funcional 
***** Funciones matematicas que regresan un numero o letra.
***** Recursividad hasta que se cumpla una condicion.
*****[#DeepSkyBlue] Lenguaje 
****** ML
****** Haskell 
****** Erlang
****[#DodgerBlue] Programacion concurrente
***** Expresa paralelismo entre tareas.
*****[#DeepSkyBlue] Lenguajes 
****** Ada
****** Java
*****[#DeepSkyBlue] Ejemplo
******_ Una cuanta vacia no puede sacar dinero.
******_ Una cuenta que hace una transaccion no\n pude realizar otra simultaneamente.
****[#DodgerBlue] Programacion distribuida 
***** Comunicacion entre computadoras
*****[#DeepSkyBlue] Programas divididos en varias computadoras
****** Abiertos 
****** Escalables 
****** Transparentes
*****[#DeepSkyBlue] Lenguajes
****** Ada 
****** E
****** Limbo
****** Oz
****[#DodgerBlue] Programacion orientada en componentes
***** Un componenete es un conjunto de objetos.
***** Utiliza la reutilizacion.
****[#DodgerBlue] Programacion orientada en aspectos 
***** Se agregan capa en base a la necesidad del programa.
***** Las capas deben ser independientes y peermitir la:
******_ Legibilidad
******_ Comodidad
****[#DodgerBlue] Programacion orientada agente software
***** Agentes que forman parte de la programacion orientada a objetos
*****[#DeepSkyBlue] Multiagentes
****** Resuelven grandes problemas, dialogos, \ncooperacion, coordinacion y negociacion.
*****[#DeepSkyBlue] Ejemplo
******_ El agente busca recomendaciones con base\n en los gustos del perfil del cliente.
@endtmindmap
```